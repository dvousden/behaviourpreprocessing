% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{loadBehaviour}
\alias{loadBehaviour}
\title{Wrapper to load behavioural data and experimental parameters.}
\usage{
loadBehaviour(x, day = NULL)
}
\arguments{
\item{x}{File to read (string). Path to the labview csv file to be loaded. This is generated with align_daq_and_dff.m and is named DAQdata2.csv}

\item{day}{Day (string) associated with labview file as specified in googlesheet. Must be one of the following: 1, 2, 3, 4, 5, omission, swap_grating, diamond.}
}
\description{
loadBehaviour reads in a decimated DAQdata2 csv file and outputs a stimulus table which identifies which experimental parameters are associated with each acquisition frame.
Briefly the steps are as follows:
\itemize{
\item Find starts of trials (see `findTrialStarts``)
\item Clean noise in tunnel position signal to identify false starts. Remove any false starts.
\item Identify types of trials (see `getTrialType``).
\item Find onset and offset of expected visual gratings (using cleaned photodiode signal).
\item Knowing the onsets/offsets, fill in the expectedVisStim for each sample.
\item Estimate onset and offset of landmark stimuli (not detected with photodiode. See \code{findLandmarks}). Landmarks are set to occur 1/3 of the way between gratings.
}

Returns a dataframe where each row is a frame and each column is a behavioural variable.
}
\details{
The visual grating presentations are determined as follows: First the photodiode signal is binarized (at a hard-coded threshold of 5).
Any signal above threshold is median filtered to remove noise. The grating onsets are determined by taking the difference between sample n and n+1 to find peaks and dips.
Then the type of visual grating is set based on the position in the tunnel (as specified in the yaml file)
For grating onsets:
\itemize{
\item position_tunnel < 1: A1 on
\item position_tunnel between 1 and 2: B1 on
\item position_tunnel between 2 and 3: A2 on
\item position_tunnel between 3 and 4.5: B2 on
\item position_tunnel between 4.5 and 5: Reward on
}

Grating offsets are specified similarly. Samples that are neither an onset or offset are 'withinStimulus'.

Processing is done using data.table package for speed. The first and last trial are excluded as these are incomplete.
}
