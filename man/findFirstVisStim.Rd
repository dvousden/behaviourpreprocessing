% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{findFirstVisStim}
\alias{findFirstVisStim}
\title{Helper function to identify the first presentation of an expected visual stimulus}
\usage{
findFirstVisStim(gf, vis)
}
\arguments{
\item{gf}{Datatable with expectedVisStim and visStimOnset}

\item{vis}{The visual stimulus you wish to detect (e.g. 'A1on')}
}
\description{
Helper function to identify the first presentation of an expected visual stimulus
}
