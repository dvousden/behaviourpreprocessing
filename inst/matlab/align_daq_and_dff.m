% Script to decimate DAQ data
% Fixes bad recordings where scanimage was started before labview
% Also writes out data to csv
%
% Dulcie & Maxime, Nov 2019

mouse = 'LP6';

%% set parameters 

nz = 4; % number of zplanes 
nbuffer = 100;
threshold = 2.5; % voltage threshold to detect frame pulse
daq_sampling = 1000; % sampling rate used in labview 
winstor_folder = '/mnt/dulciev/winstor/swc/mrsic_flogel/public/projects';

%% load datasets information for a google doc spreadsheet

datasets_infos = webread('https://docs.google.com/spreadsheets/d/10WrTfnalXPL4oLSE8nGIBA4gRQsFbY5RR1Uuf7oVGiM/gviz/tq?tqx=out:csv&gid=1620116466');

% keep only what is related to the mouse...
datasets_infos = datasets_infos(strcmp(datasets_infos.mouseName, mouse), :);

% ...and stack that where actually extracted
datasets_infos = datasets_infos(~strcmp(datasets_infos.stack, ''), :);

%% load dff data

extract_mat_file = fullfile( ...
    winstor_folder, ...
    datasets_infos.project_dir, ...
    datasets_infos.processed_data, ...
    datasets_infos.pipeline_extract);

if numel(unique(extract_mat_file)) ~= 1
    error('Arrg, all stacks results should come from the same .mat file.');
end

results = load(extract_mat_file{1}, 'dff');
dff = results.dff;  % keep original dF/F0 in results

%% align DAQ and DF/F0 data, and save DAQ results

clobber = true;

% assuming all stacks are from the same project folder
project_folder = fullfile(winstor_folder, datasets_infos.project_dir{1});

for ii = 1:height(datasets_infos)
    labview_binfolder = fullfile(project_folder, datasets_infos{ii, 'labview_path'});
    labview_binfolder = labview_binfolder{1};

    stack = datasets_infos{ii, 'stack'};
    stack = str2double(stack{1});

    fprintf('Currently working on stack %d - %s ... \n', stack, labview_binfolder);

    [DAQdata, fnames] = load_labview_daq(labview_binfolder, nbuffer);
    [DAQdata, dff] = align_daq_and_dff_data(DAQdata, dff, stack, nz, daq_sampling, threshold);
    save_daqdata(labview_binfolder, DAQdata, clobber)
    clear DAQdata;
end

%% write all ROIs traces in a HDF5 file

processed_folder = fullfile(project_folder, datasets_infos.processed_data{1});
trace_fname = fullfile(processed_folder, 'aligned_rois_traces.h5');

if ~exist(trace_fname, 'file') || clobber
    traces = rois2traces(dff);
    fprintf('Saving traces as %s ...\n', trace_fname);
    if exist(trace_fname, 'file')
        delete(trace_fname);
    end
    for ii = 1:height(datasets_infos)
        labview_binfolder = datasets_infos{ii, 'labview_path'};
        labview_key = strsplit(labview_binfolder{1}, 'abview_data');
        labview_key = labview_key{end};
        fprintf('save traces for %s\n', labview_key);
        h5create(trace_fname, labview_key, size(traces{ii}));
        h5write(trace_fname, labview_key, traces{ii});
    end
end

%% script functions

function [DAQdata2, dff] = align_daq_and_dff_data(DAQdata, dff, stack, nz, daq_sampling, threshold)
    % Fix variable name switches and contamination

    % check main inputs
    if ~exist('dff','var')==1
        error('Missing dff in environment')
    end

    if ~exist('DAQdata', 'var')
        error('Missing DAQdata argument.');
    elseif ~istable(DAQdata)
        error('Expected DAQdata to be a table.');
    elseif ~ismember('frame_pulse', DAQdata.Properties.VariableNames)
        error('Expected DAQdata table to have a frame_pulse column.');
    end

    % For data acquired after March 2019, reward and frame pulse signal are
    % switched
    if DAQdata.labview_date(1) > datetime(2019,03,01)
        fprintf('Data collected after March 2019. Correcting reward and frame pulse signal ...\n')
        DAQdata_temp = DAQdata;
        DAQdata.frame_pulse = DAQdata_temp.reward;
        DAQdata.reward = DAQdata_temp.frame_pulse;
    end

    % Fix contaminated frame-pulse
    [DAQdata_new_frame_pulse,idx] = fix_framepulse(DAQdata.frame_pulse);
    if ~isempty(idx)
        fprintf('Warning! Contaminated frame pulses fixed!\n');
    else
        fprintf('Hooray! No contaminated frame pulses detected!\n');
    end
    DAQdata = setfield(DAQdata, 'frame_pulse', DAQdata_new_frame_pulse);

    % find number of frames detected in labview (per z plane)
    % this may be a non-integer value if scanimage was stopped in the middle of
    % a zstack
    frame_pulses = diff(DAQdata.frame_pulse > threshold);
    nframes_daq = sum(frame_pulses == 1)/nz;

    % Find number of frames in dff and daq data

    % while loop just makes sure this isn't an empty roi
    roi_num = 1;
    while isempty(dff(stack,roi_num).activity)
        roi_num = roi_num + 1;
    end
    nframes_dff = length(dff(stack,roi_num).activity);
    %nframes_daq = size(DAQdata2,1);

    fprintf('I found %.2f frames in dff and %.2f frames in daq \n',nframes_dff,nframes_daq);
    fprintf('If one of these is a float, I will convert to int64 \n'); 

    % Find whether labview or scanimage was started first based on frame_pulse
    % Likewise find whether labview or scanimage was ended first

    % Time of first/last frame pulse
    last_frame_pulse = max(find(frame_pulses == 1) + 1);
    first_frame_pulse = min(find(frame_pulses == 1) + 1);

    % Time of last daq sample
    last_daq_sample = length(DAQdata.frame_pulse);

    % Compute time before first frame pulse and after last frame pulse
    time_before_first_frame_pulse = first_frame_pulse/daq_sampling;
    fprintf('There were %4.2f seconds before first frame pulse \n\n',time_before_first_frame_pulse);
    time_after_last_frame_pulse = (last_daq_sample - last_frame_pulse)/daq_sampling;
    fprintf('There were %4.2f seconds after last frame pulse \n\n',time_after_last_frame_pulse);

    % This is the expected time between pulses
    time_between_pulses = median(diff(find(frame_pulses == 1)))/daq_sampling;

    % Set thresholds to determine whether scanimage or labview started first
    % ie end_threshold is the threshold for amount of time after last frame pulse
    begin_threshold = 1.1*time_between_pulses;
    end_threshold = 10*time_between_pulses;
    % ie. if pulses are every 0.03 seconds, then if there isn't one for
    % 0.3 seconds, then scanimage was ended first

    % Determine what was started first
    if time_before_first_frame_pulse  <  begin_threshold
        started_first = 'scan_image';
    else
        started_first = 'labview';
    end

    if time_after_last_frame_pulse  > end_threshold
        ended_first = 'scan_image';
    else
        ended_first = 'labview';
    end

    fprintf('I have detected that %s was started first \n and %s was ended first\n\n',started_first,ended_first)

    % Determine situation based on number of frames and what was started first
    % Options for order of starting scanimage and labview:
    % 1) Start LV, Start SI, End SI, End LV. Then nframes_dff = nframes_daq.
    %       This is proper situation.
    % 2) Start LV, Start SI, End LV, End SI. Then nframes_dff > nframes_daq.
    %       In this scenario, the frame pulses go right to the end of the labview data
    %       we throw out the frames that occur after end of daqdata.
    %        There's no issue with decimation
    % 3) Start SI, StartLV, End LV, End SI. Then nframes_dff > nframes_daq.
    %       and there should NOT be a long period at end of daqdata with no
    %       framepulse
    %       however, it means we don't know when the daqdata occured
    %       need to use the start times.
    % 4) Start SI, Start LV, End SI, End LV. Then nframes_dff > nframes_daq
    %       and there will be a long period at end of daqdata with no
    %       framepulse.
    %       In this scenario, we align to the last frame.
    %
    % Need to have nframes_dff and nframes_daq both be integers
    nframes_dff = int64(nframes_dff);
    nframes_daq = int64(nframes_daq);
    fprintf('...Comparing number of frames in dff vs daqdata ...\n');
    fprintf('nframes_dff is %s and nframes_daqdata is %s',class(nframes_dff),class(nframes_daq));
    if nframes_dff == nframes_daq
        fprintf('Hooray! Correct number of frames in daq and dff \n');
        situation = 1;
    elseif nframes_dff > nframes_daq
        if strcmp(ended_first,'labview') && strcmp(started_first,'labview')
            situation = 2;
            fprintf('Warning! Scanimage ended after labview. I will align throw out frames on end \n');
        elseif strcmp(ended_first,'labview') &&  strcmp(started_first,'scan_image')
            situation = 3;
            fprintf('ACK! Scanimage was started before and ended after labview. \n This will be impossible to align without looking at timepoints.');
        elseif strcmp(ended_first,'scan_image') && strcmp(started_first,'scan_image')
            situation = 4;
            fprintf('Warning! Scanimage started and ended before labview. I will align datasets from last framepulse \n');
        elseif strcmp(ended_first,'scan_image') && strcmp(started_first,'labview')
            situation = 2;
            fprintf('Labview started first; Scanimage ended first. This is proper but nframes still differ.... Confusing...');
        end
    elseif int64(nframes_daq) == nframes_dff + 1
        % +1 included because there is sometimes 1 extra scanimage pulse when scanimage is shut off
        fprintf('Hooray! Correct number of frames in daq and dff (+1 extra pulse at end of daqfile when scanimage shut off)  \n');
        situation = 1;
    elseif nframes_daq > nframes_dff
        error('Ugh oh. More frames in daq than in dff. Look at this data more carefully.\n');
    end

    % Based on situation decimate the data and fix dff

    % Remove frames from dff and decimate DAQdata
    % Keep only frames that aren't dummy frames, as these won't have frame
    % pulses in daq data.
    fprintf('...Fixing dff data...\n');
    
    [~, nrois] = size(dff);
    
    if situation == 1 % frames equal, no problem.
        fprintf('Situation 1 detected.\n')
        DAQdata2 = decimate_daqdata(DAQdata, nz);
    elseif situation == 2 % no problem with decimation, remove frames at end.
        fprintf('Situation 2 detected.\n')
        DAQdata2 = decimate_daqdata(DAQdata, nz);
        for jj = 1:nrois
            if isempty(dff(stack,jj).activity)
                continue
            end
            dff(stack, jj).activity = dff(stack,jj).activity(1:nframes_daq);
        end
    elseif situation == 3
        fprintf('Situation 3 detected.\n')
        for jj = 1:nrois
            % this is worst scenario. need to look at the timepoints.
            fprintf('Again, major problem: you will need to look at scanimage and labview timepoints to figure out alignment. \n Do not proceed with analysis until then.')
        end
    elseif situation == 4
        fprintf('Situation 4 detected.\n')
        % decimate in reverse
        % Find number of dummy frames
        % There is probably a better way to do this for those more familiar with
        % structs ;-)
        nrois = size(dff,2);
        last_values = zeros(nrois,nz);
        for jj = 1:nrois
            if isempty(dff(stack,jj).activity)
                continue
            end
            last_values(jj,:) = dff(stack,jj).activity((end-nz+1):end);
        end
        % Determine number of dummy frames based on number of dff vectors with
        ndummy = 0;
        for kk = 1:nz
            if all(last_values(:,kk))
                ndummy = ndummy+1;
            end
        end
        % find start/stop of frames
        frame_pulses = diff(DAQdata.frame_pulse > threshold);
        start_idx = find(frame_pulses == 1) + 1;
        stop_idx = find(frame_pulses == -1);

        % When scanimage is stopped first, there's an extra trigger in DAQdata
        % that is not a real frame.
        %if numel(stop_idx) > numel(start_idx)
        %   stop_idx = stop_idx(1:end-1);
        %end
        % aggregate time-points of frames from different z-planes
        fprintf('Aggregating timepoints of frames from different z-planes in reverse...\n')
        nframes = length(start_idx);
        new_last = nframes - ndummy;
        reverse_start_idx = new_last:-nz:1;
        reverse_stop_idx = new_last:-nz:nz;
        start_idx = start_idx(reverse_start_idx(end:-1:1));
        stop_idx = stop_idx(reverse_stop_idx(end:-1:1));
        start_idx = start_idx(1:numel(stop_idx));

        % average time series within each frame
        colnames = DAQdata.Properties.VariableNames;
        DAQdata2 = varfun(@(x) resample_col(x, start_idx, stop_idx), DAQdata);
        DAQdata2.Properties.VariableNames = colnames;

        fprintf('Removing extra dff frames...\n');
        % keep only relevant dff data
        % keep last nframes_daq frames only, excluding dummy frames
        frames_to_keep = length(DAQdata2.frame_pulse);
        for ii = 1:nrois
            if isempty(dff(stack,ii).activity)
                continue
            else
                dff(stack,ii).activity = dff(stack,ii).activity(end-frames_to_keep-ndummy + 1:end);
            end
            % decimate in reverse
        end
    end

    % Check new variables
    fprintf('Confirming new frame length...\n');
    % Now double check new frame length
    % Note that if only 1 extra frame at the end of the daq data, I assume it's
    % the extra scanimage pulse. Maybe not ideal.
    new_nframes_dff = length(dff(stack,roi_num).activity);
    if new_nframes_dff == nframes_daq || new_nframes_dff + 1 == int64(nframes_daq)
        fprintf('Success!! Data aligned!\n\n')
    elseif new_nframes_dff == nframes_daq + ndummy || new_nframes_dff == nframes_daq + ndummy -1
        fprintf('Success!! Data aligned, but there are still dummy frames at end of dff! \n\n')
    else
        fprintf('Uh oh, data not aligned \n\n ')
    end
end

function save_daqdata(files_dir, DAQdata2, clobber)
    % save cleaned DAQdata as .mat and .csv files
  
    % save DAQ data as .mat
    daq_mat = fullfile(files_dir, 'DAQdata2.mat');
    if ~exist(daq_mat, 'file') || clobber 
        fprintf('Saving DAQ data as %s ...\n', daq_mat);
        save(daq_mat, 'DAQdata2', '-v7.3');
    else
        fprintf('Not saving DAQdata2 because clobber set to false and file exists\n');
    end

    % save DAQ data as .csv
    daq_csv = fullfile(files_dir, 'DAQdata2.csv');
    if exist(daq_csv, 'file') == 0 || clobber
        fprintf('Saving DAQ data as %s ...\n', daq_csv);
        writetable(DAQdata2, daq_csv);
    else
        fprintf('Not writing out DAQdata2 because clobber set to false and file exists\n');
    end
end

function col_data = resample_col(col_data, start_idx, stop_idx)
    % helper function to resample column data, dealing with datetime
    if isdatetime(col_data)
        col_data = arrayfun( ...
            @(x,y) mean(col_data(x:y)), start_idx, stop_idx, 'un', false);
        col_data = cat(1, col_data{:});
    else
        col_data = arrayfun(@(x,y) mean(col_data(x:y)), start_idx, stop_idx);
    end
end
